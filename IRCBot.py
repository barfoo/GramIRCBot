import sys
import os
from sys import exit
import socket
import threading
import ssl
import ConfigParser 


class IRCBot(object):

    def __init__(self):
        #config file
        self.cfg = ConfigParser.ConfigParser()
        self.cfg.read("ircbot.cfg") 
        self.botnick = self.cfg.get("bot", "nickname")
        self.channels = self.cfg.get("bot", "channels").split(',')
        self.owner = self.cfg.get("bot", "owner")
        self.server = self.cfg.get("server", "ip")
        self.port = int(self.cfg.get("server", "port"))
        #initial vars
        self.commands = [".quit", ".join", ".part", ".say"]
        self.ircmsg = ""
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.ircsock = ssl.wrap_socket(self.s)
        self.serversocket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        #init functions  
        self.connect()
        for channel in self.channels:
            self.joinchan(channel)
        self.sendowner("Im ready master")
        
        
    def connect(self):
        self.ircsock.connect((self.server, self.port))
        self.ircsock.send("USER " + self.botnick + " " + self.botnick + " " + self.botnick + "  :\r\n")
        self.ircsock.send("NICK " + self.botnick + "\r\n")

    def ping(self):
        self.ircsock.send("PONG :pingis \n")

    def sendmsg(self, msg):
        for channel in self.channels:
            self.ircsock.send("PRIVMSG " + channel + " :" + msg + "\r\n")
    
    def sendowner(self,msg):
        self.ircsock.send("PRIVMSG " + self.owner + " :" + msg + "\r\n")
      
    def joinchan(self, chan):
        self.ircsock.send("JOIN " + chan + "\n")
        if chan not in self.channels:
            self.channels.append(chan)
        
    def partchan(self, chan):
        self.ircsock.send("PART " + chan + "\n")
        if chan in self.channels:
            self.channels.remove(chan)
    
    def exitchat(self):
        self.sendowner("ByezzZzzZzz master")
        self.s.close()
        self.ircsock.close()
        exit()
    
    def analize_ircmsg(self): 
        if self.ircmsg.find("PING :") != -1:
            self.ping()
        for com in self.commands:
            if self.ircmsg.find(com) != -1:
                if self.ircmsg.find(self.botnick) != -1:
                    message = self.ircmsg.split('!')
                    if message[0] == (":" + self.owner):
                        command = self.ircmsg.split(com)
                        param = command[1]
                        param = param.strip()
                        if com == ".quit":
                            self.exitchat()
                        if com == ".say":
                            self.sendmsg(param)
                        if com == ".join":
                            self.joinchan(param)
                        if com == ".part":
                            self.partchan(param)
                            
    def listen(self):
        while 1:
            self.ircmsg = self.ircsock.recv(512)
            self.ircmsg = self.ircmsg.strip('\n\r')
            self.analize_ircmsg()
            #print(self.ircmsg)
if __name__ == "__main__":
    ircbot = IRCBot()
            

            

