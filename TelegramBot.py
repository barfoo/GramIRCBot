import telegram
import ConfigParser 
from threading import Thread
import threading
from queue import Queue
from telegram.error import NetworkError, Unauthorized
from IRCBot import IRCBot
from time import sleep
from sys import argv
from sys import exit


class TelegramBot():
    def __init__(self):
        self.groups = []
        self.cfg = ConfigParser.ConfigParser()
        self.cfg.read("telegrambot.cfg")
        self.token = self.cfg.get("bot", "token")
        self.group = self.cfg.get("bot", "group")
        self.tgbot = telegram.Bot(self.token)
        self.ircbot = IRCBot()
        w = Thread(target=self.ircbot.listen)
        w.daemon = True
        w.start()
        t = Thread(target=self.read_tg)
        #t.daemon = True
        t.start()
        print('Please wait a couple of minutes until it\'s correctly '
                  'connected')
     
    def getIRCmessages(self):
        ircmsg = self.ircbot.ircmsg
        if ircmsg.find("PRIVMSG") != -1:
            user = ircmsg.split('!')
            user = user[0]
            user = user.replace(':', '')
            ircmsg = ircmsg.split(":")
            ircmsg = ircmsg[2:]
            message = user + "-> " + ''.join(ircmsg)
            return message
        
    def getIRCUsermessage(self):
        ircmsg = self.ircbot.ircmsg
        if ircmsg.find("PRIVMSG") != -1:
            user = ircmsg.split('!')
            user = user[0]
            user = user.replace(':', '')
            return user
                
    def read_tg(self):
        update_id = 0
        chat_id = 0
        old_ircmsg = []
        old_updates = [] 
        while True:
            #print(ircmsg)
            ircmsg = self.getIRCmessages()
            user = self.getIRCUsermessage() 
            if ircmsg != old_ircmsg and ircmsg is not None:
                self.tgbot.sendMessage(self.group, ircmsg)
                old_ircmsg = ircmsg     
            try:
                for update in self.tgbot.getUpdates(offset=update_id,
                                                      timeout=10):
                    #if update is not None:
                    message = update.message.text
                    user = str(update.message.from_user.username)
                    #uncommnet this two lines for cacht the chat_id
                    #chat_id = update.message.chat_id
                    #print(chat_id)
                    # sometimes there's no user. weird, but it happens
                    if not user:
                        user = str(update.message.from_user.first_name)

                    msg = user + ": " + message
                    texttosend = user + '-> ' + message
                    if message and message not in old_updates:
                        self.ircbot.sendmsg(texttosend.encode('utf-8'))
                        old_updates.append(message)
                    
            except NetworkError as e:
                sleep(1)

            except Unauthorized:
                sleep(1)

            except Exception as e:
                print(e)
                sleep(1)


if __name__ == '__main__':
    telegrambot = TelegramBot()

    

